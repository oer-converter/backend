/**
 * Describes an error that occurred within the backend
 */
export class BackendError extends Error {
	private readonly _code: string;

	/**
	 * A code is needed to instantiate a new object
	 * @param code
	 */
	constructor(code: string) {
		super();
		this._code = code;
	}

	/**
	 * Return the error code
	 */
	get code(): string {
		return this._code;
	}
}
