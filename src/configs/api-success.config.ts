/**
 * This interface describes the config object on api errors
 */
export interface IApiSuccess {
	[key: string]: {
		httpResponseCode: number;
		httpResponseText?: string;
	}
}

/**
 * Config that describes which code (and optionally, what text) should be sent back upon successful api interaction
 */
export const ApiSuccess: IApiSuccess = {
	/**
	 * The response code for successful GET requests
	 */
	get: {
		httpResponseCode: 200
	},
	/**
	 * The response code for successful POST requests
	 */
	post: {
		httpResponseCode: 201
	},
	/**
	 * The response code for successful PUT requests
	 */
	put: {
		httpResponseCode: 204
	},
	/**
	 * The response code for successful DELETE requests
	 */
	delete: {
		httpResponseCode: 204
	}
};
