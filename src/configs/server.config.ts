/**
 * This interface describes the possible options for running this backend server
 */
interface IServerConfig {
	/**
	 * The port the http server will listen to
	 */
	port: number,
	/**
	 * The allowed origin of requests
	 */
	allowedRequestOrigin: string;
	/**
	 * The maximum size of a request body. Is roughly equivalent to the total size of the media being sent.
	 * Please refer to https://www.npmjs.com/package/bytes#bytesparsestring%EF%BD%9Cnumber-value-number%EF%BD%9Cnull
	 */
	maximumRequestBodySize: string;
}

/**
 * The actual settings
 */
export const ServerConfig: IServerConfig = {
	port: 4201,
	allowedRequestOrigin: 'http://localhost:4200',
	maximumRequestBodySize: '10mb'
};
