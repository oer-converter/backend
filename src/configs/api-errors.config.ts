/**
 * This interface describes the config object on api errors
 */
export interface IApiErrors {
	[key: string]: {
		httpResponseCode: number;
		httpResponseText: string;
	}
}

/**
 * Config that describes which code and text should be sent back upon errors
 */
export const ApiErrors: IApiErrors = {
	/**
	 * This happens when a query string tries to perform action on a db table that does not exist
	 */
	ER_NO_SUCH_TABLE: {
		httpResponseCode: 400,
		httpResponseText: 'Malformed query string. Check your query string and try again.'
	},
	ERR_INVALID_ARG_TYPE: {
		httpResponseCode: 500,
		httpResponseText: 'Internal server occured. A bug most likely. Please contact your administrator.'
	},
	ER_PARSE_ERROR: {
		httpResponseCode: 400,
		httpResponseText: 'Malformed query string. Check your query string and try again.'
	},
	UNKNOWN: {
		httpResponseCode: 500,
		httpResponseText: 'A completely unknown error has occured. Please brace yourself as the self-destruct sequence is being initiated...'
	},
	NO_NEW_INFORMATION: {
		httpResponseCode: 400,
		httpResponseText: 'The request you issued would not update any information and thus it was not processed.'
	},
	RESSOURCE_ALREADY_EXISTS: {
		httpResponseCode: 409,
		httpResponseText: 'Ressource already exists.'
	},
	NOT_IMPLEMENTED: {
		httpResponseCode: 501,
		httpResponseText: 'The request is understood but the functionality is not implemented as of yet.'
	},
	BAD_REQUEST: {
		httpResponseCode: 400,
		httpResponseText: 'The request can not be done.'
	},
	NOT_FOUND: {
		httpResponseCode: 404,
		httpResponseText: 'The requested ressource could not be found on the server.'
	},
	INVALID_ARGUMENT: {
		httpResponseCode: 400,
		httpResponseText: 'The argument(s) sent is/was not valid. Fix and try again.'
	}
};
