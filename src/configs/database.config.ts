/**
 * This interface describes the possible options and configs for using the database
 */
interface IDatabaseConfig {
	/**
	 * The type of database that is to be used. The possibilities listed here are the ones that are compatible
	 */
	service: 'mysql';
	/**
	 * The port the database instance is listening on
	 */
	port: number;
	/**
	 * The host
	 */
	host?: string;
	/**
	 * The schema that is to be used
	 */
	schema?: string;
	/**
	 * Credentials for the database user
	 */
	credentials: {
		/**
		 * The username
		 */
		name: string;
		/**
		 * The password
		 */
		password: string;
	}
}

/**
 * The actual settings
 */
export const DatabaseConfig: IDatabaseConfig = {
	service: 'mysql',
	port: 3306,
	credentials: {
		name: 'root',
		password: '***REMOVED***' // if you change this, make sure to also change it in the docker-compose file
	},
	/**
	 * When using locally for dev, 'localhost' is correct. To run via docker-compose, this needs to be changed to
	 * 'database' before creating the docker image
	 */
	host: 'localhost', //'database',
	schema: 'default'
};
