import {ServerConfig} from "./configs/server.config";
import appRest from "./rest-api.app";
import {Logger} from "./classes/logger";

/**
 * The port the server should listen on
 */
let _port = process.env.PORT || ServerConfig.port;
appRest.listen(_port, function() {
	const _version: string = require( '../package.json').version;
	Logger.log(`OER Converter Backend ${_version} is listening on :${_port} now!`);
});
