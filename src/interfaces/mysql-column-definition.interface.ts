/**
 * This is internally used by the mysql npm packet and I just created this interface to have
 * automatic code-suggestion
 */
export interface IMysqlColumnDefinition {
	_buf: ArrayBuffer;
	_clientEncoding: string | 'utf8';
	_catalogLength: number;
	_catalogStart: number;
	_schemaLength: number;
	_schemaStart: number;
	_tableLength: number;
	_tableStart: number;
	_orgTableLength: number;
	_orgTableStart: number;
	_orgNameLength: number;
	_orgNameStart: number;
	characterSet: number;
	encoding: 'binary' | string;
	name: string;
	columnLength: number;
	columnType: number;
	flags: number;
	decimals: number;
}
