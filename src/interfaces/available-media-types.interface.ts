/**
 * Describes the available media types
 */
export interface IAvailableMediaTypes {
	/**
	 * The type's id in the database
	 */
	id: number;
	/**
	 * The type as readable string
	 */
	type: string;
}
