import {IAvailableMediaTypes} from "./available-media-types.interface";
import {ILicenseTable} from "./tables/license.table.interface";
import {IReplacementMediaResponse} from "./responses/replacement-media.response.interface";
import {IMadeOerTable} from "./tables/made-oer.table.interface";
import {AcceptedTypes} from "../models/accepted-types";
import {IFeedback} from "@oer-converter/api-types";

/**
 * Describes the necessary functions a database adapter has to expose
 */
export interface IDbAdapter {
	/**
	 * Returns the id for a specific media type
	 * @param type The type who's id is being looked for
	 */
	getIdOfMediaType(type: AcceptedTypes): Promise<number>;
	/**
	 * Returns pairs of the available media types and their corresponding ID
	 * @return Promise<IAvailableMediaTypes[]> Returns pairs of the available media types and their corresponding ID
	 */
	getAvailableMediaTypes(): Promise<IAvailableMediaTypes[]>;

	/**
	 * Creates a new entry in the db for a replaced image
	 * @param _checksum The checksum of the replaced image
	 * @return Promise<number> Returns a Promise which resolves to the ID within the db table upon creation
	 */
	createReplacedImage(_checksum: string): Promise<number>;

	/**
	 * Retrieve the license object by its name
	 * @param name The name of the license to be received
	 * @param version The desired version of the named license
	 * @return Promise<ILicenseTable> A ILicenseTable object containing the license
	 */
	getLicenseByNameAndVersion(name: string, version: string): Promise<ILicenseTable>;

	/**
	 * Create a new replacing image by adding a new row to the replacingMedia table
	 * @param sourceApi The API that was used to find this image
	 * @param sourceUrl The image's path
	 * @param licenseId The ID of the license that this new image is published under
	 * @param width The width of the image
	 * @param height The height of the image
	 * @param checksum The checksum of the image
	 * @param {string=} authorUserName The user name of the author, if any
	 * @param {string=}  authorRealName The real name of the author, if any
	 * @param {string=}  archiveUrl An archived URL to the image, thus it can be found if the original path is not available anymore
	 * @return Promise<number> Returns a Promise which resolves to the ID within the db table upon creation
	 */
	createReplacingImage(
		sourceApi: string,
		sourceUrl: string,
		licenseId: number,
		width: number,
		height: number,
		checksum: string,
		authorUserName?: string,
		authorRealName?: string,
		archiveUrl?: string
	): Promise<number>;

	/**
	 * This enables to do stuff as a transaction, i.e. for complex, inter-related operations.
	 * @typeParam T The return type of the sequence that sgall be encapsulated
	 * @param encapsulatedSequence A function which performs all the things that shall all be rolled back if an error occurs anywhere
	 * @return Promise<T|void> A value that is specified by the given sequence
	 */
	encapsulateAsTransaction<T>(encapsulatedSequence: (rollback?: () => void) => Promise<T> /* This function will be called after a transaction has been begun */): Promise<T|void>;

	/**
	 * Create a new replacement action.
	 * @param replacedMediumId The ID of the medium in the 'replaced_media' table
	 * @param replacingMediumId The ID of the medium in the 'replacing_media' table
	 * @param internalPath The path inside the document's file
	 * @param docGuid The document's id in the database
	 * @return Promise<number> The new action's ID
	 */
	createReplacementAction(replacedMediumId: number, replacingMediumId: number, internalPath: string, docGuid: string): Promise<number>;

	/**
	 * Is being called when medium was published as OER. Returns the insertion id of this new action.
	 * @param docGuid The document's id in the database
	 * @param dataUrlBase64 The data url resembling the medium
	 * @param checksum The checksum, made over the data url
	 * @param width The width of the medium
	 * @param height The height of the medium
	 * @param mediaType The id of the media type
	 * @param license The id of the license it is being published under
	 * @param authorName The name of the author
	 * @param authorUrl The web appearance of the author
	 * @param authorAffiliation The author's affiliation with some institution, e.g. a chair or company
	 * @param title The work's title
	 */
	createMadeOerAction(
		docGuid: string,
		dataUrlBase64: string,
		checksum: string,
		width: number,
		height: number,
		mediaType: number,
		license: number,
		authorName?: string,
		authorUrl?: string,
		authorAffiliation?: string,
		title?: string
	): Promise<number>;

	/**
	 * Creates a new document entry. Returns the insertion id after success (which is the same as the passed GUID).
	 * @param guid The document's GUID
	 * @param mimeType The document's MIME type
	 */
	createDocument(guid: string, mimeType: string): Promise<string>;

	/**
	 * Checks whether or not a document with given GUID is known to the system
	 * @param guid The document that is being checked
	 */
	doesDocumentExist(guid: string): Promise<boolean>;

	/**
	 * Determines whether or not a given checksum relates to a medium that was already made OER.
	 * @param checksum The checksum being searched for
	 */
	doesOerAlreadyExist(checksum: string): Promise<boolean>;

	/**
	 * Returns all the media that were made OER if no type is given
	 * @param mediaType
	 */
	getMediaMadeOer(mediaType?: AcceptedTypes): Promise<IMadeOerTable[]>;

	/**
	 * The returns the medium that was made oer by a given checksum
	 * @param checksum The checksum of the desired medium
	 */
	getMediumMadeOerByChecksum(checksum: string): Promise<IMadeOerTable[]>;

	// TODO: implement getReceipt(guid: string): Promise<AAAAA>;

	/**
	 * Search for a replacing medium by its checksum
	 * @param checksum The checksum of the replacing medium to be search for
	 * @return Promise<IReplacementMediaResponse[]> This is an array, ince the algorithm used to calculate the checksum can in theory produce collisions
	 */
	getReplacementMedia(checksum: string): Promise<IReplacementMediaResponse[]>;

	/**
	 * Creates a new entry in the db for a media that derived from another replacing media by scaling, zooming, snipping etc.
	 * @param width The width of this medium
	 * @param height The height of this medium
	 * @param dataUrlBase64 A DataURL in Base64 format that contains the whole medium
	 * @param checksum The checksum of the image
	 * @param derivedFromId The id of the replacing medium this new one derived from
	 * @return Promise<number> The new row's ID in the 'editedMedia' table
	 */
	createEditedMedia(width: number, height: number, dataUrlBase64: string, checksum: string, derivedFromId: number): Promise<number>;

	/**
	 * Looks up the database for an image that matches the given checksum
	 * @param checksum The checksum of the image to be search for
	 * @return Promise<number> Returns the id of the image, if there is one. If no image is found for the given checksum, it will return -1
	 */
	getIdOfReplacedImageByChecksum(checksum: string): Promise<number>;

	/**
	 * Looks up the database for an image that matches the given checksum
	 * @param checksum The checksum of the image to be search for
	 * @return Promise<number> Returns the id of the image, if there is one. If no image is found for the given checksum, it will return -1
	 */
	getIdOfReplacingImageByChecksum(checksum: string): Promise<number>;

	/**
	 * Looks up the database for an edited variant of some media that matches the given checksum
	 * @param checksum The checksum of the edited variant of some media to be search for
	 * @param derivedFromId The id of the replacing media that it derived from
	 * @return Promise<number> Returns the id of the edited variant of some media, if there is one. If nothing is found for the given checksum, it will return -1
	 */
	getIdOfEditedVariantByChecksumAndDerivingFrom(checksum: string, derivedFromId: number): Promise<number>;

	/**
	 * Checks if this particular relation has already occured before
	 * @param replacedMediaId The id of the media to be replaced
	 * @param replacingMediaId The id of the media replacing the other one
	 * @return Promise<boolean> True, if this relation is already in the table, false if not
	 */
	wasMediaIdReplacedByMediaIdAlready(replacedMediaId: number, replacingMediaId: number): Promise<boolean>

	/**
	 * Get all the currently known feedback for the tool
	 */
	getFeedback(): Promise<IFeedback[]>;

	/**
	 * Save feedback for the tool to the database
	 * @param freeText The feedback as free text
	 * @param name The name of the feedback giver
	 */
	saveFeedback(freeText: string, name?: string): Promise<void>;
}
