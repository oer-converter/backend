/**
 * Describes an image that was declared as OER
 */
export interface IMadeImageOerRequest {
	/**
	 * The data url in base64 format
	 */
	dataUrlBase64: string;
	/**
	 * The checksum, derived from the data url
	 */
	checksum: string;
	/**
	 * The width of the image
	 */
	width: number;
	/**
	 * The height of the image
	 */
	height: number;
	/**
	 * The license under which the image was made available
	 */
	license: {
		/**
		 * The license's name
		 */
		name: string;
		/**
		 * The license's version
		 */
		version: string;
	}
	/**
	 * The author of the OER image
	 */
	author?: {
		/**
		 * The author's name
		 */
		name?: string;
		/**
		 * An url to the author's web presence
		 */
		url?: string;
		/**
		 * An institute or similar that the author is affiliated with
		 */
		affiliation?: string;
	}
	/**
	 * The title of the image
	 */
	title?: string;
	/**
	 * The document the image is in
	 */
	document: {
		/**
		 * The document's GUID
		 */
		guid: string;
		/**
		 * The document's type as official MIME type
		 */
		mimeType: string;
	}
}
