import {IMetaData} from "../meta-data.interface";

/**
 * Describes the necessary fields in the body of such a request
 */
export interface IReplaceImageRequest {
	/**
	 * Info regarding the document the action was performed on
	 */
	document: {
		/**
		 * The document's GUID
		 */
		guid: string;
		/**
		 * The internal path of the file that was replaced
		 */
		internalPath: string;
		/**
		 * The document's MIME type
		 */
		mimeType: string;
	}
	/**
	 * Info about the image that is being replaced
	 */
	replacedImage: {
		/**
		 * The checksum of the image that is being replaced
		 */
		checksum: string;
	};
	/**
	 * Info about the image that replaces
	 */
	replacingImage: {
		/**
		 * The name of the source api that was used to find this image
		 */
		sourceApi: string;
		/**
		 * The url where the image can be found
		 */
		sourceUrl: string;
		/**
		 * The author's user name, if given
		 */
		authorUserName?: string;
		/**
		 * The author's real name, if given
		 */
		authorRealName?: string;
		/**
		 * The width of the original image
		 */
		width: number;
		/**
		 * The height of the original image
		 */
		height: number;
		checksum: string;
		archiveUrl?: string;
		/**
		 * The license this image was published under
		 */
		license: {
			/**
			 * The name of the license used
			 */
			name: string;
			/**
			 * The version of the license used
			 */
			version: string;
		};
		/**
		 * If the image chosen was cropped, resized or cut-out, then this field needs to be accessible
		 */
		edited?: {
			/**
			 * The new width
			 */
			width: number;
			/**
			 * The new height
			 */
			height: number;
			/**
			 * The edited image as a data url
			 */
			dataUrlBase64: string;
			checksum: string;
		}
	}
}
