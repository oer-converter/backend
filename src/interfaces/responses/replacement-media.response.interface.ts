/**
 * Describes a replacement response
 */
export interface IReplacementMediaResponse {
	/**
	 * The source api of the replacement media
	 */
	source_api: string;
	/**
	 * The source url of the replacement media
	 */
	source_url: string;
	/**
	 * The author's real name
	 */
	author_real_name?: string;
	/**
	 * The author's username
	 */
	author_user_name?: string;
	/**
	 * The replacement's original width
	 */
	width: number;
	/**
	 * The replacement's original height
	 */
	height: number;
	/**
	 * The replacement's license name
	 */
	license_name: string;
	/**
	 * The replacement's license version
	 */
	license_version: string;
	/**
	 * The url to the deeds of the license used
	 */
	license_deeds_url: string;
	/**
	 * The edited width, if this is not the original media
	 */
	edited_width?: number;
	/**
	 * The edited height, if this is not the original media
	 */
	edited_height?: number;
	/**
	 * The edited media as data url in base64 format, if this is not the original media
	 */
	edited_data_url_base64?: number;
}
