/**
 * Describes meta data that is used by various interfaces
 */
export interface IMetaData {
	/**
	 * The width of the medium
	 */
	width: number;
	/**
	 * The height of the medium
	 */
	height: number;
	/**
	 * The checksum of the medium
	 */
	checksum: string;
}
