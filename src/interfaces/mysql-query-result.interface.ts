import * as MySQL from 'mysql2';
import {IMysqlColumnDefinition} from "./mysql-column-definition.interface";

/**
 * Describes the result of a query
 */
export interface IMysqlQueryResult<T> {
	/**
	 * The actual data that was queried
	 */
	data: T; // | MySQL.RowDataPacket[] | MySQL.RowDataPacket[][] | MySQL.OkPacket | MySQL.OkPacket[];
	/**
	 * Some further mysql info
	 */
	columns: MySQL.FieldPacket[];
}
