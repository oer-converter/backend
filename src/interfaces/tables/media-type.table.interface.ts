/**
 * The table definition for `media_type`
 */
export interface IMediaTypeTable {
	/**
	 * The id
	 */
	id: number;
	/**
	 * The type as readable string
	 */
	type: string;
}
