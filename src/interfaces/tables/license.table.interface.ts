/**
 * The table definition for `license`
 */
export interface ILicenseTable {
	/**
	 * The id
	 */
	id: number;
	/**
	 * The version
	 */
	version: string;
	/**
	 * The url to the license's deeds
	 */
	deeds_url: string;
	/**
	 * The name
	 */
	name: string;
}
