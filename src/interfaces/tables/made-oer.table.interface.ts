/**
 * The table definition for `made_oer`
 */
export interface IMadeOerTable {
	/**
	 * The id
	 */
	id: number;
	/**
	 * The data url in base64 format
	 */
	data_url_base64: string;
	/**
	 * The checksum
	 */
	checksum: string;
	/**
	 * The width
	 */
	width: number;
	/**
	 * The height
	 */
	height: number;
	/**
	 * The type's id (in the database)
	 */
	is_type: number;
	/**
	 * The license's id (in the database)
	 */
	licensed_under: number;
	/**
	 * The author's name
	 */
	author_name?: string;
	/**
	 * The url to the author's web presence
	 */
	author_url?: string;
	/**
	 * The author's affiliation, e.g. an institute
	 */
	author_affiliation?: string;
	/**
	 * The title of the medium
	 */
	title?: string;
}
