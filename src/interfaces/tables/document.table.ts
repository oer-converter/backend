/**
 * The table definition for `document`
 */
export interface IDocumentTable {
	/**
	 * The id
	 */
	id: number;
	/**
	 * The GUID
	 */
	guid: string;
	/**
	 * The official MIME type
	 */
	mime_type: string;
}
