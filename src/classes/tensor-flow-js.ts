/**
 * Credits to Prabhu at https://towardsdatascience.com/tensorflow-image-recognition-tutorial-using-serverless-architecture-node-js-69eb1a3ce110
 */
import * as tf from '@tensorflow/tfjs-node';
import * as mobilenet from '@tensorflow-models/mobilenet';
import {DataUrl} from "@hexxag0nal/data-url";
import {ITensorFlowImageClassifications} from "@oer-converter/api-types";
// import {ITensorFlowImageClassifications} from "../interfaces/tensor-flow.interface";

export class TensorFlowJs {
	constructor() {
		// tf.setBackend('tensorflow');
		tf.backend();
	}

	public async classifyImage(base64DataUrl: DataUrl): Promise<ITensorFlowImageClassifications> {
		await tf.ready();
		const model = await mobilenet.load();
		return model.classify(this._tensor3dObjectFromBase64DataUrl(base64DataUrl));
	}

	/**
	 * Creates a 3D tensor for a given base64 string
	 * @param base64DataUrl The base64 string of the image
	 * @param numOfChannels The number of channels that are to be includes
	 * @private
	 */
	private _tensor3dObjectFromBase64DataUrl(base64DataUrl: DataUrl, numOfChannels = 3): tf.Tensor3D {
		// big props to Rakesh Bhatt @ https://overflowjs.com/posts/Image-Object-Detection-Using-TensorFlowjs.html
		// extracting the pure data about the image
		const imageData = base64DataUrl.getData();//.toString().replace(/^data:([A-Za-z-+/]+);base64,/,'');
		// Uint8Array is basically a buffer (or was it the other way around?)
		const imageArray = Buffer.from(imageData, 'base64'); //toUint8Array(imageData);
		return tf.node.decodeJpeg(imageArray, numOfChannels);
	}
}
