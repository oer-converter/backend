/**
 * Exposes mechanism to control if logs are shown
 */
export class Logger {
	/**
	 * Print to the console
	 * @param msg The items to be printed onto the console
	 */
	public static log(...msg): void {
		console.log(...msg);
	}
}
