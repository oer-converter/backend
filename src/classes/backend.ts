import {Mysql} from "./mysql";
import {Request, Response} from "express";
import {IReplaceImageRequest} from "../interfaces/requests/replace-image.request.interface";
import {ApiSuccess} from "../configs/api-success.config";
import {MysqlError} from "mysql";
import {ApiErrors} from "../configs/api-errors.config";
import {IMadeImageOerRequest} from "../interfaces/requests/made-image-oer.request.interface";
import {BackendError} from "../models/backend-error";
import {TensorFlowJs} from "./tensor-flow-js";
import {DataUrl} from "@hexxag0nal/data-url";

export class Backend {
	private _dbAdapter: Mysql;
	private _tensorFlow: TensorFlowJs;

	constructor() {
		this._dbAdapter = new Mysql();
		this._tensorFlow = new TensorFlowJs();
	}

	/**
	 * Handles occuring errors. Sends back appropriate HTTP status codes and response bodies
	 * @param e The error that occurred
	 * @param res The express framework's response object
	 * @private
	 */
	private _handleError(e: BackendError | MysqlError, res: Response): void {
		console.log('Error occured:', e);
		let  _refApiError;
		if (ApiErrors.hasOwnProperty(e.code)) {
			_refApiError = ApiErrors[e.code];
		} else {
			_refApiError = ApiErrors['UNKNOWN'];
		}
		res.status(_refApiError.httpResponseCode).send({
			message: _refApiError.httpResponseText
		});
	}

	/**
	 * Send back a "Hello World" to tell that this API is reachable
	 * @param req The express framework's request object
	 * @param res The express framework's response object
	 */
	public async rootCheckAlive(req: Request, res: Response): Promise<void> {
		res.status(200).send({
			message: 'Hello World!'
		});
	}

	/**
	 * Announce a new replacing action
	 * @param req The express framework's request object
	 * @param res The express framework's response object
	 */
	public async postReplaceImage(req: Request, res: Response): Promise<void> {
		const _refReqBody: IReplaceImageRequest = req.body as IReplaceImageRequest;
		await this._dbAdapter.encapsulateAsTransaction(async (rollback): Promise<void> => {
			const _docId: string = _refReqBody.document.guid;
			// check if document's guid is known
			// let _docId = await this._dbAdapter.getDocumentIdByGuid(_refReqBody.document.guid);
			if (!(await this._dbAdapter.doesDocumentExist(_docId))) {
				// then the document is not known to the db yet and needs to be created
				await this._dbAdapter.createDocument(_docId, _refReqBody.document.mimeType);
			}

			let _replacedImageId = await this._dbAdapter.getIdOfReplacedImageByChecksum(_refReqBody.replacedImage.checksum);
			// if the result is -1, then the image does not exist yet and needs to be created
			if (_replacedImageId === -1) {
				_replacedImageId = await this._dbAdapter.createReplacedImage(_refReqBody.replacedImage.checksum);
			}

			const _licenseId = (await this._dbAdapter.getLicenseByNameAndVersion(_refReqBody.replacingImage.license.name, _refReqBody.replacingImage.license.version)).id;
			let _editedMediaId: number = null;

			let _replacingImageId = await this._dbAdapter.getIdOfReplacingImageByChecksum(_refReqBody.replacingImage.checksum);
			// if the result is -1, then the image does not exist yet and needs to be created.
			if (_replacingImageId === -1) {
				_replacingImageId = await this._dbAdapter.createReplacingImage(
					_refReqBody.replacingImage.sourceApi,
					_refReqBody.replacingImage.sourceUrl,
					_licenseId,
					_refReqBody.replacingImage.width,
					_refReqBody.replacingImage.height,
					_refReqBody.replacingImage.checksum,
					_refReqBody.replacingImage?.authorUserName,
					_refReqBody.replacingImage?.authorRealName,
					_refReqBody.replacingImage?.archiveUrl
				);
			}

			// if an edited variant was provided and it is not already in the db, then create it
			if (
				_refReqBody.replacingImage.edited &&
				(await this._dbAdapter.getIdOfEditedVariantByChecksumAndDerivingFrom(_refReqBody.replacingImage.edited.checksum, _replacingImageId)) === -1
			) {
				const _refEdited = _refReqBody.replacingImage.edited;
				_editedMediaId = await this._dbAdapter.createEditedMedia(_refEdited.width, _refEdited.height, _refEdited.dataUrlBase64, _refEdited.checksum, _replacingImageId);
			}

			if (!(await this._dbAdapter.wasMediaIdReplacedByMediaIdAlready(_replacedImageId, _replacingImageId))) {
				const _linkId = await this._dbAdapter.createReplacementAction(_replacedImageId, _replacingImageId, _refReqBody.document.internalPath, _docId);
			}
		}).catch((reason: MysqlError) => {
			this._handleError(reason, res);
		});

		res.status(ApiSuccess['post'].httpResponseCode).send();
	}

	/**
	 * Return the images that replaced an image with the given checksum
	 * @param req The express framework's request object
	 * @param res The express framework's response object
	 */
	public async getReplacedImages(req: Request, res: Response): Promise<void> {
		const _replacements = await this._dbAdapter.getReplacementMedia(req.params['checksum']);
		res.status(ApiSuccess['get'].httpResponseCode).send(_replacements);
	}

	/**
	 * Announce a new action, that an image was declared as OER
	 * @param req The express framework's request object
	 * @param res The express framework's response object
	 */
	public async postMadeImageOer(req: Request, res: Response): Promise<void> {
		const _reqBody: IMadeImageOerRequest = req.body as IMadeImageOerRequest;
		// Dont care about the below since md5 could in theory produce collisions
		/*if (await this._dbAdapter.doesOerAlreadyExist(_reqBody.checksum)) {
			// if checksum is already known, assume that this image was already made oer
			const _errorCode = 'RESSOURCE_ALREADY_EXISTS';
			const _err: BackendError = new BackendError(_errorCode);
			// res.status(_err.httpResponseCode).send('Checksum already known. Can\' create anew');
			this._handleError(_err, res);
		} else {*/
			if (!(await this._dbAdapter.doesDocumentExist(_reqBody.document.guid))) {
				await this._dbAdapter.createDocument(_reqBody.document.guid, _reqBody.document.mimeType);
			}
			const _imageTypeId = await this._dbAdapter.getIdOfMediaType('image');
			const _licenseId = (await this._dbAdapter.getLicenseByNameAndVersion(_reqBody.license.name, _reqBody.license.version)).id;
			await this._dbAdapter.createMadeOerAction(
				_reqBody.document.guid,
				_reqBody.dataUrlBase64,
				_reqBody.checksum,
				_reqBody.width,
				_reqBody.height,
				_imageTypeId,
				_licenseId,
				_reqBody.author ? _reqBody.author.name : null,
				_reqBody.author ? _reqBody.author.url : null,
				_reqBody.author ? _reqBody.author.affiliation : null,
				_reqBody.title
			);
		// }
		res.status(ApiSuccess['post'].httpResponseCode).send();
	}

	/**
	 * Get the images that were made OER with a given checksum
	 * @param req The express framework's request object
	 * @param res The express framework's response object
	 */
	public async getImageMadeOer(req: Request, res: Response): Promise<void> {
		const img = await this._dbAdapter.getMediumMadeOerByChecksum(req.params['checksum']);
		if (img) {
			res.status(ApiSuccess['get'].httpResponseCode).send(img);
		} else {
			this._handleError(new BackendError('NOT_FOUND'),res);
		}
	}




	public async getImageClassifications(req: Request, res: Response): Promise<void> {
		const dataUrlString = req.body['dataUrlBase64'];
		// const dataUrlString = req.params['dataUrlBase64'];
		// console.log('data url', dataUrlString);
		if (DataUrl.isValidDataUrlString(dataUrlString)) {
			res.status(ApiSuccess['get'].httpResponseCode).send(await this._tensorFlow.classifyImage(new DataUrl(dataUrlString)));
		} else {
			this._handleError(new BackendError('INVALID_ARGUMENT'), res);
		}
	}


	/**
	 * Get feedback
	 */
	public async getFeedback(req: Request, res: Response): Promise<void> {
		res.status(ApiSuccess['get'].httpResponseCode).send(await this._dbAdapter.getFeedback());
	}

	/**
	 * Save feedback
	 */
	public async saveFeedback(req: Request, res: Response): Promise<void> {
		const text: string = req.body['freeText'];
		const name: string = req.body['name'];
		try {
			await this._dbAdapter.saveFeedback(text, name);
			res.status(ApiSuccess['post'].httpResponseCode).send()
			// @ts-ignore
		} catch (e: Error) {
			this._handleError(new BackendError(e.name), res);
		}
	}
}
