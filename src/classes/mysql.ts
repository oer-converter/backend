import * as MySQL from 'mysql2/promise'
import {DatabaseConfig} from "../configs/database.config";
import {IMysqlQueryResult} from "../interfaces/mysql-query-result.interface";
import {Logger} from "./logger";
import {IMediaTypeTable} from "../interfaces/tables/media-type.table.interface";
import {IAvailableMediaTypes} from "../interfaces/available-media-types.interface";
import {ILicenseTable} from "../interfaces/tables/license.table.interface";
import {IReplacementMediaResponse} from "../interfaces/responses/replacement-media.response.interface";
import {IDbAdapter} from "../interfaces/db-adapter.interface";
import {IDocumentTable} from "../interfaces/tables/document.table";
import {AcceptedTypes} from "../models/accepted-types";
import {IMadeOerTable} from "../interfaces/tables/made-oer.table.interface";
import {BackendError} from "../models/backend-error";
import {IFeedbackResponse} from "@oer-converter/api-types";

/**
 * This class implements the necessary functions for a mysql solution
 */
export class Mysql implements IDbAdapter{
	private _connection: MySQL.Connection;

	constructor() {
	}

	/**
	 * Allows for sending queries to the mysql database by using prepared statements to prevent mysql injections
	 * @param queryString The query that shall be executed
	 * @param values The values that shall be inserted into the query
	 * @private
	 */
	private async _query<TableContentInterface>(queryString: string, ...values): Promise<IMysqlQueryResult<TableContentInterface>> {
		let _connection: MySQL.Connection;
		let _closeConnectionSubsequently = false;
		// if no global connection is given at runtime, then a new connection should be opened and closed afterwards
		if (this._connection === null || this._connection === undefined) {
			_connection = await this._connect();
			this._connection = _connection;
			_closeConnectionSubsequently = true;
		} else {
			_connection = this._connection;
		}
		const [rows, fields] = await _connection.execute(queryString, values);
		if (_closeConnectionSubsequently) {
			this._disconnect();
		}
		return {
			data: rows as unknown as TableContentInterface,
			columns: fields
		}
	}

	/**
	 * Creates a connection to the database
	 * @private
	 */
	private async _connect(): Promise<MySQL.Connection> {
		return MySQL.createConnection({
			user: DatabaseConfig.credentials.name,
			password: DatabaseConfig.credentials.password,
			host: DatabaseConfig.host,
			database: DatabaseConfig.schema,
			// insecureAuth: true
		});
	}

	/**
	 * The beginning of a mysql transaction
	 * @private
	 */
	private async _beginTransaction(): Promise<void> {
		this._connection = await this._connect();
		return this._connection.beginTransaction();
	}

	/**
	 * Committing and thus ending a mysql transaction
	 * @private
	 */
	private async _commitTransaction(): Promise<void> {
		// use old style then/catch chaining because somehow using await and .catch did not work as expected
		this._connection.commit()
			.then(async value => {
				this._disconnect();
		})
			.catch(async reason => {
				Logger.log('Committing transaction failed, here\'s why: ', reason, 'Automatically rolling back changes!');
				await this._connection.rollback();
				this._disconnect();
		});
	}

	/**
	 * Rollback the current transaction
	 * @private
	 */
	private async _rollbackTransaction(): Promise<void> {
		await this._connection.rollback();
	}

	/**
	 * Close the current connection
	 * @private
	 */
	private async _disconnect(): Promise<void> {
		await this._connection.end();
		this._connection = null;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async getIdOfMediaType(type: 'image' | 'video'): Promise<number> {
		Logger.log(`Getting id of media type '${type}'.`);
		return (await this._query<IMediaTypeTable[]>('SELECT * FROM `media_type` WHERE `type` = ? LIMIT 1;', 'image')).data.pop().id;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async getAvailableMediaTypes(): Promise<IAvailableMediaTypes[]> {
		Logger.log(`Returning all available media types.`);
		const result = await this._query('SELECT * FROM media_type;');
		return result.data as IAvailableMediaTypes[];
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async createReplacedImage(_checksum: string): Promise<number> {
		Logger.log(`Creating a replaced image via checksum ${_checksum}.`);
		const _mediaTypeId = await this.getIdOfMediaType('image');
		const id = (await this._query<MySQL.OkPacket>('INSERT INTO replaced_media (checksum, is_type) VALUES (?, ?);', _checksum, _mediaTypeId)).data.insertId;
		Logger.log(`Successfully created a replaced image via checksum ${_checksum} with id ${id}.`);
		return id;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async getLicenseByNameAndVersion(name: string, version: string): Promise<ILicenseTable> {
		Logger.log(`Getting license id of ${name}, ${version}.`);
		return (await this._query<ILicenseTable[]>('SELECT * FROM `license` WHERE `name` = ? AND `version` = ? LIMIT 1;', name, version)).data.pop();
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async createReplacingImage(
		sourceApi: string,
		sourceUrl: string,
		licenseId: number,
		width: number,
		height: number,
		checksum: string,
		authorUserName?: string,
		authorRealName?: string,
		archiveUrl?: string
	): Promise<number> {
		Logger.log(`Creating a replacing image.`);
		const _mediaTypeId = await this.getIdOfMediaType('image');
		const id = (await this._query<MySQL.OkPacket>(
			'INSERT INTO replacing_media (source_url, author_real_name, source_api, author_user_name, is_type, licensed_under, height, width, checksum, archive_url) VALUES (?,?,?,?,?,?,?,?,?,?)',
			sourceUrl, authorRealName || null, sourceApi, authorUserName || null, _mediaTypeId, licenseId, height, width, checksum, archiveUrl || null)).data.insertId;
		Logger.log(`Successfully created a replacing image with id ${id}.`);
		return id;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async encapsulateAsTransaction<T>(encapsulatedSequence: (rollback?: () => void) => Promise<T> /* This function will be called after a transaction has been begun */): Promise<T|void> {
		await this._beginTransaction();
		Logger.log(`Starting transaction wrapper.`);
		let _errorOccured = false;
		const result = await encapsulatedSequence(this._rollbackTransaction).catch(reason => {
			Logger.log('Error occured during transaction: ', reason);
			_errorOccured = true;
			return Promise.reject(reason);
		});
		if (!_errorOccured) {
			await this._commitTransaction();
		}
		Logger.log(`Closing transaction wrapper.`);
		return result;
	}

	/**
	 * Creates a new action and returns the id
	 * @private
	 */
	private async _createAction(documentId: string): Promise<number> {
		Logger.log(`Creating a new action and linking it to document with id ${documentId}.`);
		return (await this._query<MySQL.OkPacket>('INSERT INTO action (document) VALUES (?);', documentId)).data.insertId;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */ // TODO: TEST
	public async getDocumentIdByGuid(guid: string): Promise<number> {
		Logger.log(`Getting document's id by GUID ${guid}.`);
		const _result: IDocumentTable[] = (await this._query<IDocumentTable[]>('SELECT * FROM document WHERE guid = ? LIMIT 1;', guid)).data;
		if (_result.length === 0) {
			return -1; // indicate that the GUID is not known
		} else {
			return _result[0].id;
		}
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async createDocument(guid: string, mimeType: string): Promise<string> {
		Logger.log(`Creating document of type ${mimeType} with GUID ${guid}.`);
		return (await this._query<MySQL.OkPacket>('INSERT INTO document (guid, mime_type) VALUES (?,?);',
			guid,
			mimeType
		)).data.insertId.toString();
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async doesDocumentExist(guid: string): Promise<boolean> {
		Logger.log(`Checking if document with GUID ${guid} exists.`);
		const _result: IDocumentTable[] = (await this._query<IDocumentTable[]>('SELECT * FROM document WHERE guid = ? LIMIT 1;', guid)).data;
		console.log('result', _result);
		return _result.length !== 0;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async createReplacementAction(replacedMediumId: number, replacingMediumId: number, internalPath: string, docGuid: string): Promise<number> {
		Logger.log(`Linking replaced image with id ${replacedMediumId} being replaced by replacing image with id ${replacingMediumId}.`);
		const _actionId: number = await this._createAction(docGuid);
		return (await this._query<MySQL.OkPacket>(
				'INSERT INTO replacement (id, id_replaced_media, id_replacing_media, internal_path) VALUES (?,?, ?,?);',
				_actionId,
				replacedMediumId,
				replacingMediumId,
				internalPath
			)
		).data.insertId;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async createMadeOerAction(
		docGuid: string,
		dataUrlBase64: string,
		checksum: string,
		width: number,
		height: number,
		mediaType: number,
		license: number,
		authorName?: string,
		authorUrl?: string,
		authorAffiliation?: string,
		title?: string
	): Promise<number> {
		Logger.log(`Creating a new MadeOerAction.`);
		const _actionId: number = await this._createAction(docGuid);
		return (await this._query<MySQL.OkPacket>(
			'INSERT INTO made_oer (id, data_url_base64, checksum, width, height, is_type, licensed_under, author_name, author_url, author_affiliation, title) VALUES (?,?,?,?,?,?,?,?,?,?,?);',
			_actionId,
			dataUrlBase64,
			checksum,
			width,
			height,
			mediaType,
			license,
			authorName || null,
			authorUrl || null,
			authorAffiliation || null,
			title || null
		)).data.insertId
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async getMediaMadeOer(mediaType?: AcceptedTypes): Promise<IMadeOerTable[]> {
		Logger.log(`Getting media made oer.`);
		let _q = 'SELECT * FROM made_oer ;';
		if (mediaType) {
			switch (mediaType) {
				case "image":
					const _imgTypeId = await this.getIdOfMediaType(mediaType);
					_q += ' WHERE is_type = ?';
					break;
				case "video":
					throw new BackendError('NOT_IMPLEMENTED');
				default:
					throw new BackendError('BAD_REQUEST');
			}
		}
		// add the missing ; at the end of the query
		_q += ';';

		return (await this._query<IMadeOerTable[]>(_q, mediaType)).data;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async getMediumMadeOerByChecksum(checksum: string): Promise<IMadeOerTable[]> {
		Logger.log(`Get medium made oer with checksum ${checksum}.`);
		const _result = await this._query<IMadeOerTable[]>('SELECT made_oer.id as id, data_url_base64, checksum, width, height, media_type.type as media_type, author_name, author_url, author_affiliation, title, license.name as license_name, license.version as license_version, license.deeds_url as license_deeds FROM made_oer JOIN license on made_oer.licensed_under = license.id JOIN media_type on made_oer.is_type = media_type.id WHERE checksum = ?;', checksum);
		if (_result.data.length === 0) {
			return null
		} else {
			return _result.data;
		}
	}


	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async getReplacementMedia(checksum: string): Promise<IReplacementMediaResponse[]> {
		Logger.log(`Returning all replacements for replaced media with checksum ${checksum}.`);
		const q = 'SELECT replacing_media.source_api,\n' +
			'       replacing_media.source_url,\n' +
			'       replacing_media.author_real_name,\n' +
			'       replacing_media.author_user_name,\n' +
			'       replacing_media.width,\n' +
			'       replacing_media.height,\n' +
			'       license.name as license_name,\n' +
			'       license.version as license_version,\n' +
			'       license.deeds_url as license_deeds_url,\n' +
			'       edited_media.width as edited_width,\n' +
			'       edited_media.height as edited_height,\n' +
			'       edited_media.data_url_base64 as edited_data_url_base64\n' +
			'FROM replaced_media\n' +
			'    JOIN replacement ON replaced_media.id = replacement.id_replaced_media\n' +
			'    JOIN replacing_media ON replacing_media.id = replacement.id_replacing_media\n' +
			'    JOIN license ON replacing_media.licensed_under = license.id\n' +
			'    JOIN media_type ON replacing_media.is_type = media_type.id\n' +
			'    LEFT JOIN edited_media ON replacing_media.id = edited_media.derived_from\n' +
			'WHERE\n' +
			'      replaced_media.checksum = ? AND\n' +
			'      media_type.type = ?;\n';
		return (await this._query<IReplacementMediaResponse[]>(q, checksum, 'image')).data;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async createEditedMedia(width: number, height: number, dataUrlBase64: string, checksum: string, derivedFromId: number): Promise<number> {
		Logger.log(`Creating an edited media`);
		const id = (await this._query<MySQL.OkPacket>('INSERT INTO edited_media (width, height, data_url_base64, checksum, derived_from) VALUES (?,?,?,?,?);',
			width,
			height,
			dataUrlBase64,
			checksum,
			derivedFromId)).data.insertId;
		Logger.log(`Successfully created an edited media with id ${id}`);
		return id;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async getIdOfReplacedImageByChecksum(checksum: string): Promise<number> {
		const _idOfImageMediaType = await this.getIdOfMediaType('image');
		const qResult = await this._query<{id: number}[]>('SELECT id FROM replaced_media WHERE checksum = ? AND is_type = ? LIMIT 1;', checksum, _idOfImageMediaType);
		if (qResult.data.length > 0) {
			const id = qResult.data.pop().id;
			Logger.log(`found replaced image an image with checksum ${checksum}, it has id ${id}`);
			return id;
		} else {
			Logger.log(`Could not find any replaced image with checksum ${checksum}`);
			return -1;
		}
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async getIdOfReplacingImageByChecksum(checksum: string): Promise<number> {
		const _idOfImageMediaType = await this.getIdOfMediaType('image');
		const qResult = await this._query<{id: number}[]>('SELECT id FROM replacing_media WHERE checksum = ? AND is_type = ? LIMIT 1;', checksum, _idOfImageMediaType);
		if (qResult.data.length > 0) {
			const id = qResult.data.pop().id;
			Logger.log(`found a replacing image with checksum ${checksum}, it has id ${id}`);
			return id;
		} else {
			Logger.log(`Could not find any replacing image with checksum ${checksum}`);
			return -1;
		}
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async getIdOfEditedVariantByChecksumAndDerivingFrom(checksum: string, derivedFromId: number): Promise<number> {
		const qResult = await this._query<{id: number}[]>('SELECT id FROM edited_media WHERE checksum = ? AND derived_from = ? LIMIT 1;',
			checksum, derivedFromId);
		if (qResult.data.length > 0) {
			const id = qResult.data.pop().id;
			Logger.log(`found edited variant of some media with checksum ${checksum}, it has id ${id}`);
			return id;
		} else {
			Logger.log(`Could not find any edited variant of some media with checksum ${checksum}`);
			return -1;
		}
	}

	/*public async isEqualReplacicngImage(params: IIsEqualReplacingImage): Promise<boolean> {
		const qString = 'SELECT id FROM replacing_media WHERE id = ? AND checksum = ? AND licensed_under = ? AND width = ? AND height = ? AND source_url = ? AND source_api = ? AND author_real_name = ? AND author_user_name = ? AND archive_url = ? LIMIT 1;';
		const qResult = await this._mysql.query<{id: number}[]>(qString, params.id, params.checksum, params.licenseId,
			params.width, params.height, params.source_url, params.source_api, params?.author_real_name,
			params?.author_user_name, params?.archive_url);
		// return true if the result is not empty and the result's id matches the requested one
		// Of course it ought to do that, otherwise the result ought to be emtpy, but just making sure
		return (qResult.data.length > 0 && qResult.data.pop()?.id === params.id);
	}*/

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async wasMediaIdReplacedByMediaIdAlready(replacedMediaId: number, replacingMediaId: number): Promise<boolean> {
		return (await this._query<{id: number}[]>('SELECT id_replaced_media as id FROM replacement WHERE id_replaced_media = ? AND id_replacing_media = ? LIMIT 1;',
			replacedMediaId, replacingMediaId)).data.length > 0;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async doesOerAlreadyExist(checksum: string): Promise<boolean> {
		return (await this._query<[]>('SELECT checksum FROM made_oer WHERE checksum = ? LIMIT 1;', checksum)).data.length > 0;
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async getFeedback(): Promise<IFeedbackResponse[]> {
		const data = (await this._query<(IFeedbackResponse & {free_text: string;})[]>('SELECT name, time, free_text FROM feedback')).data;
		const transformData = (data: (IFeedbackResponse & {free_text: string;})[]): IFeedbackResponse[] => {
			for (const feedback of data) {
				feedback.freeText = feedback.free_text;
				feedback.free_text = undefined;
				delete feedback.free_text;
			}
			return data;
		}
		return transformData(data);
	}

	/**
	 * For documentation and explanation, see the IDbAdapter interface
	 */
	public async saveFeedback(freeText: string, name?: string): Promise<void> {
		await this._query('INSERT INTO feedback (free_text, name) VALUES (?,?)', freeText, name || 'unknown');
	}


}
