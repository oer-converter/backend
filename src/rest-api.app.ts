import * as express from "express";
import * as bodyParser from "body-parser";
import {Backend} from "./classes/backend";
import * as CORS from "cors";
import {ServerConfig} from "./configs/server.config";

/**
 * Describes an API object
 */
class RestApiApp {
	/**
	 * Instantiation of the backend
	 */
	private _backend: Backend;
	/**
	 * CORS options for the whole object
	 */
	private _corsOptions;

	/**
	 * Instantiate the API object
	 */
	constructor() {
		this.app = express();
		this._backend = new Backend();
		this.config();
		this.routes();
		this._corsOptions = {
			origin: ServerConfig.allowedRequestOrigin,
			optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
		};
	}

	/**
	 * The actual API object
	 */
	public app: express.Application;

	/**
	 * Configure the API with CORS etc.
	 */
	private config(): void {
		this.app.use(bodyParser.json({
			limit: ServerConfig.maximumRequestBodySize
		}));
		this.app.use(bodyParser.urlencoded({ extended: true }));
		this.app.use(CORS(this._corsOptions));
	}

	/**
	 * Define the routes and methods the API should listen on
	 */
	private routes(): void {
		const router = express.Router();

		router.options('*', CORS(this._corsOptions));

		router.get('/', this._backend.rootCheckAlive);

		router.post('/replace/image', this._backend.postReplaceImage.bind(this._backend), CORS(this._corsOptions));

		router.get('/replace/image/:checksum', this._backend.getReplacedImages.bind(this._backend));

		router.post('/oer/image', this._backend.postMadeImageOer.bind(this._backend), CORS(this._corsOptions));

		router.get('/oer/image/:checksum', this._backend.getImageMadeOer.bind(this._backend));

		// this violates the RESTful paradigm, but sending a base64 data url (even if it's url-safe) does get rejected,
		// probably because the request header is too large then
		router.post('/recognize/image', this._backend.getImageClassifications.bind(this._backend));

		router.get('/feedback', this._backend.getFeedback.bind(this._backend));

		router.post('/feedback', this._backend.saveFeedback.bind(this._backend));

		// TODO: add route for digesting a receipt

		// TODO: add route to get receipt for document with given GUID

		this.app.use('/', router);
	}
}

/**
 * Export the api such that it can be used and started by the server
 */
export default new RestApiApp().app;
