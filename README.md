[![logo](https://lubna_ali81.pages.rwth-aachen.de/ba-webtool-oer/docs/backend/images/coverage-badge-documentation.svg?sanitize=true "Backend's documentation coverage")](https://lubna_ali81.pages.rwth-aachen.de/ba-webtool-oer/docs/backend/coverage.html)

# Backend
The backend will provide the API and the database.

The backend is intended to run as a NodeJs application.

# API
You can find the documentation of the API over [here](https://documenter.getpostman.com/view/3974771/SzRw1Vzx?version=latest)