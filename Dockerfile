# extra metadata
#LABEL version="0.1"
#LABEL description="Image for the OER Tool Backend"

# In this image, we transpile the typescript to javascript
# We previously used an Alpine linux image but tensorflow seems to have some issues with ith even when rebuilding the
FROM node:12.13 as node
ENV DIR=/usr/src/app
WORKDIR $DIR
# copy the package.json and the ts.config into the image
COPY package.json $DIR
COPY tsconfig.json $DIR
# Now copy the .npmrc to install private packages from the correct source and set it via cmd
RUN npm config set @oer-converter:registry https://git.rwth-aachen.de/api/v4/packages/npm/
COPY .npmrc $DIR
# then run npm install in productive mode to install all dependencies of the project
RUN npm i  --production

## dev stuff
#RUN apk update
#RUN apt-get update
#RUN apk add build-essential
#RUN apt-get install build-essential
#RUN apk add python
#RUN apt-get install python
#RUN uname -a
#RUN npm install -g node-gyp
#RUN node-gyp configure --verbose


# see https://github.com/tensorflow/tfjs/issues/2370#issuecomment-553995625 to fix tensorflow issue
#RUN npm rebuild @tensorflow/tfjs-node --build-from-source




# Add all the content to the WORKDIR
ADD . /usr/src/app
# finally transpile the source
RUN npx tsc --outDir $DIR/dist
# RUN the server
CMD ["node","./dist/server.js"]
# Expose the port to make communication with the API from the outside world possible
EXPOSE 4201



#FROM node:12.13-alpine as node2
#ENV DIR=/usr/src/app
#WORKDIR $DIR
#COPY --from=node $DIR/dist $DIR
#CMD ["node","/usr/src/app/server.js"]
